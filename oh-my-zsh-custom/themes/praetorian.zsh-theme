# Based on gnzh theme

# load some modules
autoload -U colors zsh/terminfo # Used in the colour alias below
colors
setopt prompt_subst


# Gets the difference between the local and remote branches
function local_git_status() {
    if [ -f 'no-git-status' ] ; then
        echo ""
        return 0
    fi
    local git_status remote
    local current_branch=$(git_current_branch)
    remote=${$(command git rev-parse --verify ${hook_com[branch]}@{upstream} --symbolic-full-name 2>/dev/null)/refs\/remotes\/}
    local ret=$?
    if [[ $ret != 0 ]]; then
        git_status=${current_branch}

        if [[ ! -z $git_status ]]; then
            git_status="$ZSH_THEME_GIT_PROMPT_PREFIX${git_status}$ZSH_THEME_GIT_PROMPT_SUFFIX"
        fi
    else
        git_status="$ZSH_THEME_GIT_PROMPT_PREFIX${current_branch}%{$PR_NO_COLOR%}${$(git_remote_status)#$ZSH_THEME_GIT_PROMPT_PREFIX$remote}"
    fi
    echo $git_status$(git_prompt_status)
}


# make some aliases for the colours: (could use normal escape sequences too)
for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
  eval PR_$color='%{$fg[${(L)color}]%}'
done
eval PR_NO_COLOR="%{$terminfo[sgr0]%}"
eval PR_BOLD="%{$terminfo[bold]%}"

# Check the UID
if [[ $UID -ne 0 ]]; then # normal user
  eval PR_USER='${PR_GREEN}%n${PR_NO_COLOR}'
  eval PR_USER_OP='${PR_GREEN}%#${PR_NO_COLOR}'
  local PR_PROMPT='$PR_NO_COLOR➤ $PR_NO_COLOR'
else # root
  eval PR_USER='${PR_RED}%n${PR_NO_COLOR}'
  eval PR_USER_OP='${PR_RED}%#${PR_NO_COLOR}'
  local PR_PROMPT='$PR_RED➤ $PR_NO_COLOR'
fi

# Check if we are on SSH or not
if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then
  eval PR_HOST='${PR_YELLOW}%M${PR_NO_COLOR}' #SSH
else
  eval PR_HOST='${PR_GREEN}%M${PR_NO_COLOR}' # no SSH
fi

local return_code="%(?..%{$PR_RED%}%? ↵%{$PR_NO_COLOR%})"

local user_host='${PR_USER}${PR_CYAN}@${PR_HOST}'
local current_dir='%{$PR_BOLD$PR_BLUE%}%~%{$PR_NO_COLOR%}'
local git_status='$(local_git_status)'
local current_time='%{$PR_CYAN%}[%D{%H:%M:%S}]%{$PR_NO_COLOR%}'

PROMPT="╭─${user_host} ${current_time} ${current_dir} ${git_status}%{$PR_NO_COLOR%}
╰─$PR_PROMPT"
RPS1="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$PR_YELLOW%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$PR_YELLOW%}› %{$PR_NO_COLOR%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$PR_CYAN%}*"
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_GIT_PROMPT_EQUAL_REMOTE=" ↔"
ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE=" ↑"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE=" ↓"
ZSH_THEME_GIT_PROMPT_DIVERGED_REMOTE=" ↯"

ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE_COLOR="%{$PR_MAGENTA%}"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE_COLOR="%{$PR_RED%}"
ZSH_THEME_GIT_PROMPT_REMOTE_STATUS_DETAILED=1

ZSH_THEME_GIT_PROMPT_REMOTE_STATUS_PREFIX="%{$PR_YELLOW%}‹"
ZSH_THEME_GIT_PROMPT_REMOTE_STATUS_SUFFIX="%{$PR_YELLOW%}› %{$PR_NO_COLOR%}"

ZSH_THEME_GIT_PROMPT_ADDED="%{$PR_GREEN%}✚"
ZSH_THEME_GIT_PROMPT_AHEAD="%{$PR_MAGENTA%}↑"
ZSH_THEME_GIT_PROMPT_BEHIND="%{$PR_RED%}↓"
ZSH_THEME_GIT_PROMPT_DELETED="%{$PR_RED%}✖"
ZSH_THEME_GIT_PROMPT_DIVERGED="%{$PR_BLUE%}↯"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$PR_BLUE%}✹"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$PR_MAGENTA%}➜"
ZSH_THEME_GIT_PROMPT_STASHED="%{$PR_GREEN%}‡"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$PR_YELLOW%}✭"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$PR_CYAN%}?"
