# @(#).cshrc	1.12 (Qualcomm) 16 Sep 1994
#
# Copyright (C) 1994 Qualcomm Incorporated
#
# Comment: Default .cshrc for GrapeVine users 
#
# Usage: executed by csh, before the .login  
#
# Known Bugs: 
#
# Author: Lorayne Witte, Lee Damon
#
#############################################################################
#############################################################################
##     DO NOT MAKE ADDITIONS TO THIS FILE. IF YOU WISH TO CHANGE YOUR      ##
##     ENVIRONMENT, CREATE AND EDIT A .cshrc.local FILE IN YOUR HOME       ##
##     DIRECTORY. CALL HOTLINE AT x1099 IF YOU NEED HELP WITH THIS.        ##
#############################################################################
#############################################################################
#

#
# the following two calls make it so we can have a global environment, with
# global initializations of things like prompts, path, manpath and such,
# while allowing you to customize things you like. You don't have to
# worry about us changing your local things, because we just change
# the global files, and stay out of your home directory.
# 

#
# source the global .cshrc file, if it exists
#
# this gives you the Qualcomm standard initializations, calls initializations
# based on which groups you are a member of, then calls your subscribed package
# initializations. To see what is happening, just look at the files. If you
# have questions, call Hotline at x-1099.
#
if ( -f /usr/local/etc/cshrc.global ) then
	source /usr/local/etc/cshrc.global
endif

#
# source the user's local .cshrc file, if it exists
#
# This is where you can set your own initializations. You can chose to change
# what we've done for you above, or add to it. Most people will have a set
# of aliases they want, and .cshrc.local is the place to put them.
#
if ( -f ~/.cshrc.local ) then
	source ~/.cshrc.local
endif
