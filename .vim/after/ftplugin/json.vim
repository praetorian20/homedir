" Space instead of tabs & tab width
setlocal shiftwidth=4
setlocal tabstop=4
setlocal softtabstop=4
setlocal expandtab
setlocal autoindent
setlocal fileformat=unix