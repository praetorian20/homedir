" Call clang-format before writing buffer

function! s:ClangFormatFile()
   if filereadable("/opt/tbs/tbs-llvm-clang-3.8.0/root/usr/share/clang/clang-format.py")
      let l:lines="all"
      pyf /opt/tbs/tbs-llvm-clang-3.8.0/root/usr/share/clang/clang-format.py
   endif
endfunction
command! ClangFormatFile call <SID>ClangFormatFile()


au BufWritePre <buffer> ClangFormatFile

