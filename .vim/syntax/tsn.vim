" Quit if a syntax file is already loaded
if exists("b:current_syntax")
   finish
endif

" Read XML syntax
runtime! syntax/xml.vim
unlet b:current_syntax

" Next read C syntax
runtime! syntax/c.vim
unlet b:current_syntax

" Keywords
syn keyword tsnKeyword import macro expand align
syn keyword tsnKeyword reserve nextgroup=cNumbers skipwhite

let b:current_syntax = "tsn"

hi def link tsnKeyword  Keyword
