" Quit if a syntax file is already loaded
if exists("b:current_syntax")
   finish
endif

" Read Markdown syntax
"runtime! syntax/markdown.vim
"unlet! b:current_syntax

syn match datetime "^-- [0-9]\{4}/[0-9]\{2}/[0-9]\{2} [0-9]\{2}:[0-9]\{2}:[0-9]\{2} --$"

syn region codeBlock start="```" end="```" contains=@NoSpell
syn region inlineCode start="`" end="`" contains=@NoSpell

hi def link datetime Delimiter
hi def link inlineCode Keyword
hi def link codeBlock Keyword

let b:current_syntax = "journal"
