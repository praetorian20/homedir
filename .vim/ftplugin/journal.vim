" Enable spellcheck
setlocal spell

setlocal noexpandtab

iabbrev <silent> date; -- <C-R>=strftime("%Y/%m/%d %H:%M:%S")<CR> --