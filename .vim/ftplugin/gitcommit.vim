" Enable spellcheck
setlocal spell

" Always open on first line
call setpos('.', [0, 1, 1, 0])