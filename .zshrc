#!/bin/zsh
# -*- Mode: text -*-
#======================================================================
# FILE: zshrc
#
# SERVICES: Personal zshrc file
#
# DESCRIPTION:
#
# @@-COPYRIGHT-START-@@
#
#     COPYRIGHT 2004-2018 QUALCOMM INCORPORATED.  All rights reserved.
#                QUALCOMM proprietary and confidential.
#
# Use of this copyrighted software is strictly conditioned upon Recipient's
# agreement to be bound by the terms and conditions detailed in the
# accompanying LICENSE.TXT file.
#
# Recipients of this software hereby affirmatively agree to be bound by all
# terms and conditions set forth in the agreement between Recipient and
# QUALCOMM (the "Agreement").
#
# @@-COPYRIGHT_END-@@
#
#======================================================================

if [[ -f /prj/tbs/lte/modem/main/sw/build/scripts/env ]]; then
   source /prj/tbs/lte/modem/main/sw/build/scripts/env
fi
if [[ -f /opt/rh/devtoolset-3/enable ]]; then
   source /opt/rh/devtoolset-3/enable
fi

unset CCACHE_DISABLE
PATH="$HOME/bin:$PATH:/prj/tbs/lte/modem/main/users/asadanan/scripts"
export PATH

if [[ ! -o interactive ]]
then
        return
fi

#automatically hash commands for quicker retrieval.
#type 'hash' to find out what has been hashed
setopt hashcmds

# PROMPT='[(Z) %n@%m %l <%c> ]%# '
# PROMPT='%B%m%b> '
# RPROMPT="[%{$fg_no_bold[yellow]%}%~%{$reset_color%} %D{%H:%M:%S}]"

## smart urls
autoload -U url-quote-magic
zle -N self-insert url-quote-magic

#bindkey binds keystrokes to commands.
#it uses the VISUAL or EDITOR variable
# to use either vi or emacs mode for the zle (line editor)
#keybindings can be overridded if necessary
# see the zshzle man page for details

#force zle to use emacs bindings always
#bindkey -e

#bindkey '^Xk' copy-region-as-kill

#
# Set up completion
#
#completion control. If you do cd ^D, you should get a list of dirs only

autoload -U compinit
compinit -u

zmodload -i zsh/complist

# styles

#user colors for selection
zstyle ':completion:*::::default' list-colors ''

zstyle ':completion:*' menu select=2

zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''

#
# Function Declarations:
#

ka ()    { kill -9 `/bin/ps augxww | grep $1 | grep $2 | egrep -v grep | awk '{print $2}'` }
s_env()  { source ${HOME}/.zshenv }
s_rc()   { source ${HOME}/.zshrc }
setenv() { export $1=$2 }

#year=`date +%Y`
#sourceenscript()    { echo enscript -C -G -r  -b"(c) $year QUALCOMM Inc./QUALCOMM Proprietary" $1 }
#sourceenscripttwo() { echo enscript -C -G -r2 -b"(c) $year QUALCOMM Inc./QUALCOMM Proprietary" $1 }

host=$(hostname)

export HISTSIZE=10000
export DISTCC_HOSTS=localhost
export BTMAKEJ=4

# P4 settings
export P4PORT=aswp401:1666
export P4CONFIG=.p4config
export P4USER=$USER
export P4CLIENT=asadanan_octopus
export P4HOST=$host

# ccache settings
if [[ "$host" == 'ltm-dev-'* || "$host" == *'-lnx' ]]; then
  if [[ -f /usr/bin/ccache ]]; then
     export CCACHE_DIR="/local/mnt/workspace/${USER}/.ccache"
     export CCACHE_BASEDIR=/local/mnt/workspace/${USER}
     unset CCACHE_HASHDIR
     ccache -M 10G
  fi

  alias p4='/pkg/qct/software/perforce/current/p4'
  alias p4v='/pkg/qct/software/perforce/current/p4v'
  alias p4merge='/pkg/qct/software/perforce/current/p4v/bin/p4merge'
  alias tmux='/usr/local/bin/tmux'

  # License for TotalView
  # setenv LM_LICENSE_FILE `/pkg/qct/software/v2p/bin/v2p roguewave`

elif [[ "$host" == 'loreley' ]]; then
  PATH="$PATH:/pkg/icetools/bin"
  export PATH

fi

# Bindings for home, end, delete, insert keys
bindkey '\e[1~'   beginning-of-line  # Linux console
bindkey '\e[H'    beginning-of-line  # xterm
bindkey '\eOH'    beginning-of-line  # gnome-terminal
bindkey '\e[2~'   overwrite-mode     # Linux console, xterm, gnome-terminal
bindkey '\e[3~'   delete-char        # Linux console, xterm, gnome-terminal
bindkey '\e[4~'   end-of-line        # Linux console
bindkey '\e[F'    end-of-line        # xterm
bindkey '\eOF'    end-of-line        # gnome-terminal
bindkey '\e[1;5D' backward-word
bindkey '\e[1;5C' forward-word

export EDITOR=vim
export SVN_EDITOR=vim

# Adjacent duplicate commands are not shown in history
setopt HIST_IGNORE_DUPS

# # turn off tracing
# unsetopt xtrace
# # restore stderr to the value saved in FD 3
# exec 2>&3 3>&-

# oh-my-zsh setup
export ZSH=~/.oh-my-zsh/
ZSH_CUSTOM=~/oh-my-zsh-custom/
ZSH_THEME="praetorian"
DISABLE_AUTO_UPDATE="true"
if [[ "$host" == 'loreley' ]]; then
  # loreley (which runs SUSE) seems to have problems with zsh-syntax-highlighting
  plugins=(git)
  source $ZSH/oh-my-zsh.sh

else
  if [[ -d $ZSH ]]; then
    plugins=(git zsh-syntax-highlighting)

    source $ZSH/oh-my-zsh.sh
    ZSH_HIGHLIGHT_STYLES[path]='fg=cyan'
    ZSH_HIGHLIGHT_STYLES[path_prefix]='none'
    ZSH_HIGHLIGHT_STYLES[path_approx]='fg=yellow,bold'
    ZSH_HIGHLIGHT_STYLES[globbing]='fg=yellow'
  fi
fi

# Prevent auto rename of tmux panes
DISABLE_AUTO_TITLE=true

# Color output for gcc
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;34:locus=01;33:quote=01;32'

alias vi='vim'
alias ll='ls -al'
alias make='make -j`nproc`'

alias dir='ls -al'
alias type='less'
alias ldir='ls -alF1 | grep /'
alias make_tags='ctags --c++-kinds=+p --fields=+iaSl --extra=+q --language-force=C++ -f .tags -R . --exclude=runtime --exclude=pkg --exclude=thirdParty/boost --exclude=thirdParty/tsnc --exclude=thirdParty/wireshark > /dev/null 2>&1 .tags'
alias subbuild='bsub -q priority -R "select[type==LINUX64] rusage[mem=6500]" '
alias subprior='bsub -q priority -R "select[type==LINUX64] rusage[mem=1000]" '
alias gl='git log1'
alias gla='git log2'
if [ -e "~/smartless/smartless" ] ; then
   alias less='~/smartless/smartless'
fi

# Setup ssh-agent
if [[ -f ~/.ssh/id_rsa ]]; then
   eval `ssh-agent -s`
   #ssh-add ~/.ssh/id_rsa
fi

#
# End .zshrc
#
