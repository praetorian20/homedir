" Necessary for lots of cool vim things
set nocompatible

scriptencoding utf-8
set encoding=utf-8

" Vundle setup
filetype off                  " required
" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'Valloric/YouCompleteMe'
Plugin 'Yggdroot/indentLine'
Plugin 'airblade/vim-gitgutter'
Plugin 'easymotion/vim-easymotion'
Plugin 'godlygeek/tabular'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'luochen1990/rainbow'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'nvie/vim-flake8'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'othree/xml.vim'
Plugin 'plasticboy/vim-markdown'
Plugin 'python-mode/python-mode'
Plugin 'scrooloose/nerdcommenter.git'
Plugin 'scrooloose/nerdtree.git'
Plugin 'sukima/xmledit'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tmux-plugins/vim-tmux-focus-events'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-git'
Plugin 'troydm/easybuffer.vim'
Plugin 'vim-scripts/DoxygenToolkit.vim'
Plugin 'wesQ3/vim-windowswap'
Plugin 'wincent/command-t'

" End configuration, makes the plugins available
call vundle#end()

" This shows what you are typing as a command
set showcmd

" Set color scheme
if &diff
  colorscheme jellybeans
else
  if has('win32') && has("gui_win32")
    colorscheme koehler
  else
    colorscheme jellybeans
  endif
endif

" Set font
if has('win32') && has("gui_win32")
  set guifont=Hack:h8:cANSI
else
  set guifont=Hack\ 8
endif

" Folding stuff
set foldmethod=marker

" Highlight search
set hlsearch

" Incremental search
set incsearch

" Enable autoindent
set autoindent
set smartindent

" Space instead of tabs & tab width
set shiftwidth=3
set tabstop=3
set softtabstop=3
set expandtab
set smarttab

" Highlight current line
set cursorline

" Make backspace work like most other apps
set backspace=2

" Add newline to end of file
set eol

" Single space after period
set nojoinspaces

" No backups
set nobackup

" Parentheses matching
set showmatch

" Write temporary backup files
set writebackup

" Increase highlighting accuracy
syn sync fromstart

" utf-8 encoding
set encoding=utf-8

" Pattern matching for GCC's C++ header highlighting
augroup gcc_hdr_hi
   au BufRead * if search('-*- C++ -*-', 'nw') | setlocal ft=cpp | endif
augroup END

" Remove toolbar
set guioptions-=T

if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window.
  if has('win32')
    au GUIEnter * simalt ~x
  else
    set lines=999 columns=999
  endif
endif

" Word selection
nmap <C-S-Left> vbge<Space>
nmap <C-S-Right> vew<BS>
imap <C-S-Left> _<Esc>mz"_xv`z<BS>obge<Space>
imap <C-S-Right> _<Esc>my"_xi<S-Right><C-o><BS>_<Esc>mz"_xv`yo`z
vmap <C-S-Left> bge<Space>
vmap <C-S-Right> ew<BS>

" Down/up selection
nmap <C-S-Down> v<Down>
nmap <C-S-Up> v<Up>
imap <C-S-Down> _<Esc>mz"_xv`zo`z<Down><Right><BS><BS>
imap <C-S-Up> _<Esc>mz"_xv`z<Up>o`z<BS>o
vmap <C-S-Down> <Down>
vmap <C-S-Up> <Up>

" home/end selection
nmap <C-S-Home> v<Home>
nmap <C-S-End> v<End>
imap <C-S-Home> _<Esc>mz"_s<C-o><Left><C-o>`z<Esc>v<Home>
imap <C-S-End> _<Esc>mz"_xv`zo<End>

" half page down/up selection
nmap <C-S-PageDown> v<End><C-d><End>
nmap <C-S-PageUp> v<Home><C-u>
imap <C-S-PageDown> _<Esc>mz"_xv`zo<End><C-d><End>
imap <C-S-PageUp> _<Esc>mz"_xv`z<BS>o<Home><C-u>
vmap <C-S-PageDown> <End><C-d><End>
vmap <C-S-PageUp> <Home><C-u>

" Search for word under cursor
:nnoremap <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>

" Word deletion
imap <C-BS> <C-w>
imap <C-Del> _<Esc>mzew<BS>i<Del><Esc>v`z"_c

" Enable plugins
set nocp
filetype plugin indent on

" Enable syntax highlighting
syntax enable

" Display line numbers
set number

" Display whitespace characters
set list
set listchars=tab:▸-,trail:•,nbsp:¬,extends:»,precedes:«

" Avoids error when executing cscope_maps.vim plugin
if has("cscope")
  set nocscopeverbose
endif

" ctags setup
set tags=./.tags,.tags;/

" Insert in normal mode
:nmap <Space> i_<Esc>r

" Ignore whitespace in diff mode
if &diff
  " Diff mode
  set diffopt+=iwhite
endif

" Preview window at the bottom
function! PreviewDown()
   if !&previewwindow
       silent! wincmd P
   endif
   if &previewwindow
       silent! wincmd J
       silent! wincmd p
   endif
endf
augroup preview_down_group
   au BufWinEnter * call PreviewDown()
augroup END

set nowritebackup
set nobackup
set noswapfile

" Powerline setup
if &diff
else
   if isdirectory("/opt/rh/python27/root/usr/lib/python2.7/site-packages/powerline/bindings/vim")
      set rtp+=/opt/rh/python27/root/usr/lib/python2.7/site-packages/powerline/bindings/vim
   endif
   if isdirectory("/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim")
      set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
   endif
   set t_Co=256
   set laststatus=2      " Always show statusline
   let g:Powerline_symbols = "fancy"
endif

" Read local vimrc
set exrc
set secure

" indentLine setup
let g:indentLine_color_term = 235
let g:indentLine_color_gui = '#A4E57E'
let g:indentLine_char = '┊'
let g:indentLine_faster = 1

" YCM setup
set completeopt+=longest,menu
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_always_populate_location_list = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_collect_identifiers_from_tag_files = 1
let g:ycm_confirm_extra_conf = 0
let g:ycm_enable_diagnostic_highlighting = 1
let g:ycm_enable_diagnostic_signs = 1
let g:ycm_extra_conf_globlist = ['~/.vim']
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_key_list_select_completion = ['<TAB>', '<Down>']
let g:ycm_register_as_syntastic_checker = 0
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_show_diagnostics_ui = 1
if filereadable("/opt/rh/python27/root/usr/bin/python2.7")
   let g:ycm_python_binary_path = '/opt/rh/python27/root/usr/bin/python2.7'
   let g:ycm_server_python_interpreter = '/opt/rh/python27/root/usr/bin/python2.7'
endif
if filereadable("/usr/bin/python2.7")
   let g:ycm_python_binary_path = '/usr/bin/python2.7'
   let g:ycm_server_python_interpreter = '/usr/bin/python2.7'
endif
nnoremap <leader>gt :YcmCompleter GoTo<CR>
nnoremap <leader>gd :YcmCompleter GoToDefinitionElseDeclaration<CR>

" NERDTree setup
let g:NERDTreeDirArrows = 1
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
map <Leader>n <plug>NERDTreeTabsToggle<CR>

" EasyBuffer setup
noremap <Leader>be :EasyBuffer<CR>

" Rainbow setup
let g:rainbow_active = 1

" flake8 Python linting
if filereadable("/opt/rh/python27/root/usr/bin/flake8")
   let g:flake8_cmd = "/opt/rh/python27/root/usr/bin/flake8"
endif
if filereadable("/usr/local/bin/flake8")
   let g:flake8_cmd = "/usr/local/bin/flake8"
endif
let g:flake8_show_in_gutter = 1
let g:flake8_show_in_file = 1

" python-mode setup
let g:pymode_lint_on_write = 0
let g:pymode_folding = 0
let g:pymode_rope_lookup_project = 0
let g:pymode_rope_regenerate_on_write = 0

" clang-format setup
if filereadable("/opt/tbs/tbs-llvm-clang-3.8.0/root/usr/bin/clang-format")
   let g:clang_format_path = "/opt/tbs/tbs-llvm-clang-3.8.0/root/usr/bin/clang-format"
endif
if filereadable("/usr/bin/clang-format")
   let g:clang_format_path = "/usr/bin/clang-format"
endif
let g:clang_format_fallback_style = "none"

" Line number colors
highlight LineNr ctermfg=grey guifg=#c0c0c0
highlight CursorLineNe ctermfg=yellow guifg=#ffff00

" Column highlighting
augroup insert_mode
   let blacklist = ['py']
   au InsertEnter * if index(blacklist, &ft) < 0 | setlocal colorcolumn=80,100,120
   au InsertLeave * if index(blacklist, &ft) < 0 | setlocal colorcolumn=
augroup END
highlight ColorColumn ctermbg=Black guibg=#202020

" Current line highlighting
highlight Cursor cterm=bold ctermbg=Black guifg=#303030
highlight CursorLine ctermbg=Black guibg=#202020

" Toggle paste mode with F10
set pastetoggle=<F10>

" Change cursor in insert mode
" http://vim.wikia.com/wiki/Change_cursor_shape_in_different_modes
augroup insert_mode
   au VimEnter,InsertLeave * silent execute '!echo -ne "\e[1 q"' | redraw!
   au InsertEnter,InsertChange *
            \ if v:insertmode == 'i' |
            \   silent execute '!echo -ne "\e[5 q"' | redraw! |
            \ elseif v:insertmode == 'r' |
            \   silent execute '!echo -ne "\e[3 q"' | redraw! |
            \ endif
   au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
augroup END

" Set ignore wildcards for Command-T
set wildignore=*.o,*.obj,*.swp,*.bak,*.pyc,*.class,*.jar,*.gif,*.png,*.jpg,runtime/**,thirdParty/*/current_version/**
